.globl binary_search
binary_search:

SUB sp, sp, #28 //// adjusts stack to make room for 5 items

STR R4, [sp, #0] //// //stores the R4 in a variable register on the stack since R3 -R0 and R12 are the only scratch registers. R4 will contain middleIndex 
STR R5, [sp, #4] //// stores R5 for use later. R5 will contain keyIndex
STR R6, [sp, #8] //// stores R6 for use later. R6 will contain numbers[middleIndex]
STR R7, [sp, #12] //// stores R7 for use later. R7 will contain -NumCalls
STR R9, [sp, #16] //// stores R9 for use later. R9 will contain Numcalls
STR R10, [sp, #20] //// stores R10 for use later.
STR lr, [sp, #24] //// save the return address


////middleIndex = startIndex + (endIndex - startIndex)/2//

SUB R4, R3, R2 //// this is the (endIndex-startIndex)
 //// this means that we are dividing the contents of R4 by 2. ie (endIndex-startIndex)/2
ADD R4, R2, R4, LSR #1  //// middleIndex (R4) now contains middleIndex = startIndex + (endIndex - startIndex)/2//

////int keyIndex
//// STR R5, [sp, #4]

//// NumCalls++
ADD R8, R8, #1 //// R9 will get R9 +1 (NumCalls++)

//// compares startIndex and endIndex
CMP R2, R3
//// branch to first if statement
BGT L1
//if (startIndex > endIndex)


LDR R6, [R0, R4, LSL #2] //// numbers[middleIndex] = R6

CMP R6, R1 //// compares numbers[middleIndex] with key

//// branch to else if statement
BEQ L2

//// branch to second else if statement
BGT L3

// else
//// keyIndex = binary_search(numbers, key, middleIndex+1, endIndex, NumCalls)//

B L4 

//else 
L4: 
ADD R4, R4, #1 // middleIndex = middleIndex +1

MOV R3, R4 // puts middleIndex into R3 which contains endIndex
// All other arguments should stay the same, meaning that we can BL back to binary_search

BL binary_search //// calls the function recursively with middleIndex for endIndex


L1: 

LDR R4, [sp, #0] //// //stores the R4 in a variable register on the stack since R3 -R0 and R12 are the only scratch registers. R4 will contain middleIndex 
LDR R5, [sp, #4] //// stores R5 for use later. R5 will contain keyIndex
LDR R6, [sp, #8] //// stores R6 for use later. R6 will contain numbers[middleIndex]
LDR R7, [sp, #12] //// stores R7 for use later. R7 will contain -NumCalls
LDR R9, [sp, #16] //// stores R9 for use later. R9 will contain Numcalls
LDR R10, [sp, #20] //// stores R10 for use later.
LDR lr, [sp, #24] //// save the return address
ADD sp, sp, #28 // pop 5 items off the stack

MOV R0, #-1 //return -1

MOV pc, lr // return to the caller


// else if (numbers[middleIndex] == key)
L2: 
MOV R5, R4// keyIndex = middleIndex
B cont

//else if (numbers[middleIndex] > key)
L3: 
SUB R4, R4, #1 // middleIndex = middleIndex -1
MOV R3, R4 

// keyIndex = binary_search(numbers, key, middleIndex-1, endIndex, NumCalls)//

BL binary_search // calls the function recursively with middleIndex for endIndex

cont: 
MOV R10, #0 // 
RSB R6, R10, R6 // reverse subtract 0 from NumCalls = -NumCalls
STR R6, [R0, R4, LSL #2] // numbers [middlendex] = -NumCalls

MOV R0, R5 // Moving keyIndex into numbers to be displayed in LEDs

LDR R4, [sp, #0] //// //stores the R4 in a variable register on the stack since R3 -R0 and R12 are the only scratch registers. R4 will contain middleIndex 
LDR R5, [sp, #4] //// stores R5 for use later. R5 will contain keyIndex
LDR R6, [sp, #8] //// stores R6 for use later. R6 will contain numbers[middleIndex]
LDR R7, [sp, #12] //// stores R7 for use later. R7 will contain -NumCalls
LDR R9, [sp, #16] //// stores R9 for use later. R9 will contain Numcalls
LDR R10, [sp, #20] //// stores R10 for use later.
LDR lr, [sp, #24] //// save the return address
ADD sp, sp, #28 // adjusts stack to pop 5 items 
MOV pc, lr // return to caller












